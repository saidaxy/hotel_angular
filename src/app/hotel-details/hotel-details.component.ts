import { Hotel } from '../hotel';
import { Component, OnInit, Input } from '@angular/core';
import { HotelService } from '../hotel.service';
import { HoteListComponent } from '../hotel-list/hote-list.component';

@Component({
  selector: 'app-hotel-details',
  templateUrl: './hotel-details.component.html',
  styleUrls: ['./hotel-details.component.css']
})
export class HotelDetailsComponent implements OnInit {

  @Input() hotel: HoteListComponent;

  constructor(private hotelService: HotelService, private listComponent: HoteListComponent) { }

  ngOnInit() {
  }

}
