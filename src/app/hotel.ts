export class Hotel {
  id: number;
  name: string;
  city: string;
  street: string;
  description: string;
  price: string;
}
