import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import {Observable} from 'rxjs';
import {Hotel} from '../hotel';
import {HotelService} from '../hotel.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  hotels: Observable<Hotel[]>;

  constructor(private hotelService: HotelService,
              private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.hotels = this.hotelService.getHotelsList();
  }

  deleteHotel(id: number) {
    this.hotelService.deleteHotel(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
}
